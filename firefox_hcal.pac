// PAC file for firefox (uses SOCKS5, not SOCKS)
function FindProxyForURL(url, host) {

    if (shExpMatch(url,"*.cms904*")) {
        return "SOCKS5 127.0.0.1:2508";
    }
    else if (shExpMatch(url,"*.cms*")) {
        return "SOCKS5 127.0.0.1:2507";
    }
    else if (shExpMatch(url,"*cmsrc-*:*")) {
        return "SOCKS5 127.0.0.1:2507";
    }
    else if (shExpMatch(url,"*dqm-prod-local*:*")) {
    	return "SOCKS5 127.0.0.1:2507";
    }
    else if (isInNet(host, "172.16.0.0",  "255.255.0.0"))    {
        return "SOCKS5 127.0.0.1:2507";
    }
    else if (isInNet(host, "10.176.0.0",  "255.255.0.0"))    {
        return "SOCKS5 127.0.0.1:2507";
    }
    else if (shExpMatch(url,"*srv-*:*")){
	return "SOCKS5 127.0.0.1:2507";
    }
    // All other requests go directly to the WWW:
    return "DIRECT";
}

