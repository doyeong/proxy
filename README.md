## Where to save

Firstly, save the ```firefox_hcal.pac``` and ```config``` which are stored in the repository to the following area. 

```
/Users/doyeongkim/firefox_hcal.pac
~/.ssh/config 
```

Click :apple: on top left of your monitor and navigate to ```System Preferences > Network > Advanced > Proxies```.

Choose **Automatic Proxy Configuration**.

Enter the following to Proxy Configuration File URL.

file:///Users/doyeongkim/firefox_hcal.pac

## How to tunnel

From your terminal,

```
ssh -AtXY doyeong@lxplus.cern.ch -L2508:localhost:2508 "ssh -AtXY -D 2508 doyeong@cms904usr -4"
ssh -AtXY doyeong@lxplus.cern.ch -L2507:localhost:2507 "ssh -AtXY -D 2507 doyeong@cmsusr -4"
```
